"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var common_1 = require("@angular/common");
var core_1 = require("@angular/core");
var sohoxi_angular_1 = require("@infor/sohoxi-angular");
var lime_1 = require("lime");
var CSICustomerComponent = /** @class */ (function () {
    function CSICustomerComponent() {
        this.logPrefix = "[CSICustomerComponent] ";
    }
    CSICustomerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        var settings = this.widgetContext.getSettings();
        var instance = this.widgetInstance;
        instance.settingsOpening = function (options) {
            _this.logInfo("settingsOpening");
        };
        instance.settingsSaved = function () {
            _this.loadData();
        };
        this.loadData();
    };
    CSICustomerComponent.prototype.loadData = function () {
        var _this = this;
        this.setBusy(true);
        var request = this.createRequest();
        this.widgetContext
            .executeIonApiAsync(request)
            .subscribe(function (response) { return (_this.customerList = response.data.Items); }, function (error) { return _this.showErrorMessage(error); });
        this.setBusy(false);
    };
    CSICustomerComponent.prototype.createRequest = function () {
        if (location.hostname === "localhost") {
            return {
                method: "GET",
                url: "/infor.demo.csi.customer/data.json",
                cache: false,
                headers: {
                    Accept: "application/json",
                    "X-Infor-MongooseConfig": "CSI_Demo_Dals"
                }
            };
        }
        else {
            return {
                method: "GET",
                url: "/MONGOOSE/IDORequestService/MGRestService.svc/json/SLCustomers/CustNum,Name,Addr_1",
                cache: false,
                headers: {
                    Accept: "application/json",
                    "X-Infor-MongooseConfig": "CSI_Demo_Dals"
                }
            };
        }
    };
    CSICustomerComponent.prototype.showErrorMessage = function (error) {
        lime_1.Log.error(this.logPrefix + "ION API Error: " + JSON.stringify(error));
        this.setBusy(false);
        this.messageService
            .error({
            title: "Error " + error.status,
            message: "Failed to call ION API",
            buttons: [
                {
                    text: "Close",
                    isDefault: true
                }
            ]
        })
            .open();
    };
    CSICustomerComponent.prototype.setBusy = function (isBusy) {
        // Show the indeterminate progress indicator when the widget is busy by changing the widget state.
        this.widgetContext.setState(isBusy ? lime_1.WidgetState.busy : lime_1.WidgetState.running);
    };
    CSICustomerComponent.prototype.onRendered = function (event) {
        this.logInfo("Rendered listview: " + event);
    };
    CSICustomerComponent.prototype.onSelected = function (event) {
        this.logInfo("Selected item: " + event);
    };
    CSICustomerComponent.prototype.onSorted = function (event) {
        this.logInfo("Sorted: " + event);
    };
    CSICustomerComponent.prototype.logInfo = function (message, ex) {
        lime_1.Log.info(this.logPrefix + message, ex);
    };
    __decorate([
        core_1.Input()
    ], CSICustomerComponent.prototype, "widgetContext");
    __decorate([
        core_1.Input()
    ], CSICustomerComponent.prototype, "widgetInstance");
    CSICustomerComponent = __decorate([
        core_1.Component({
            template: "\n\t<div>\n\t\t<soho-listview\n\t\t\t(rendered)=\"onRendered($event)\"\n\t\t\t(selected)=\"onSelected($event)\"\n\t\t\t(sorted)=\"onSorted($event)\">\n\t\t\t<li soho-listview-item *ngFor=\"let item of customerList\">\n\t\t\t\t<p soho-listview-header>{{item[1]}}</p>\n\t\t\t\t<p soho-listview-subheader>{{item[2]}}</p>\n\t\t\t</li>\n\t\t</soho-listview>\n\t</div>"
        })
    ], CSICustomerComponent);
    return CSICustomerComponent;
}());
exports.CSICustomerComponent = CSICustomerComponent;
var CSICustomerModule = /** @class */ (function () {
    function CSICustomerModule() {
    }
    CSICustomerModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, sohoxi_angular_1.SohoListViewModule],
            declarations: [CSICustomerComponent],
            entryComponents: [CSICustomerComponent]
        })
    ], CSICustomerModule);
    return CSICustomerModule;
}());
exports.CSICustomerModule = CSICustomerModule;
