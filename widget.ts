import { IWidgetContext2, IWidgetInstance2 } from "lime";
import { CSICustomerComponent, CSICustomerModule } from "./main";

export const widgetFactory = (context: IWidgetContext2): IWidgetInstance2 => {
  return {
    angularConfig: {
      moduleType: CSICustomerModule,
      componentType: CSICustomerComponent
    }
  };
};
