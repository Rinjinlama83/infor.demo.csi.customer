"use strict";
exports.__esModule = true;
var main_1 = require("./main");
exports.widgetFactory = function (context) {
    return {
        angularConfig: {
            moduleType: main_1.CSICustomerModule,
            componentType: main_1.CSICustomerComponent
        }
    };
};
