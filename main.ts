import { CommonModule } from "@angular/common";
import { AfterViewInit, Component, Input, NgModule } from "@angular/core";
import { SohoListViewModule } from "@infor/sohoxi-angular";
import {
  IWidgetComponent,
  IWidgetContext2,
  IWidgetInstance2,
  IWidgetSettingsArg,
  Log,
  WidgetState,
  IIonApiRequestOptions
} from "lime";
import { HttpErrorResponse } from "@angular/common/http";

@Component({
  template: `
	<div>
		<soho-listview
			(rendered)="onRendered($event)"
			(selected)="onSelected($event)"
			(sorted)="onSorted($event)">
			<li soho-listview-item *ngFor="let item of customerList">
				<p soho-listview-header>{{item[1]}}</p>
				<p soho-listview-subheader>{{item[2]}}</p>
			</li>
		</soho-listview>
	</div>`
})
export class CSICustomerComponent implements AfterViewInit, IWidgetComponent {
  @Input()
  widgetContext: IWidgetContext2;
  @Input()
  widgetInstance: IWidgetInstance2;
  customerList: any;
  private logPrefix = "[CSICustomerComponent] ";
  messageService: any;

  ngAfterViewInit() {
    const settings = this.widgetContext.getSettings();

    const instance = this.widgetInstance;

    instance.settingsOpening = (options: IWidgetSettingsArg) => {
      this.logInfo("settingsOpening");
    };

    instance.settingsSaved = () => {
      this.loadData();
    };

    this.loadData();
  }

  loadData() {
    this.setBusy(true);
    const request = this.createRequest();
    this.widgetContext
      .executeIonApiAsync<any>(request)
      .subscribe(
        response => (this.customerList = response.data.Items),
        error => this.showErrorMessage(error)
      );
    this.setBusy(false);
  }

  private createRequest(): IIonApiRequestOptions {
    if (location.hostname === "localhost") {
      return {
        method: "GET",
        url: "/infor.demo.csi.customer/data.json",
        cache: false,
        headers: {
          Accept: "application/json",
          "X-Infor-MongooseConfig": "CSI_Demo_Dals"
        }
      };
    } else {
      return {
        method: "GET",
        url:
          "/MONGOOSE/IDORequestService/MGRestService.svc/json/SLCustomers/CustNum,Name,Addr_1",
        cache: false,
        headers: {
          Accept: "application/json",
          "X-Infor-MongooseConfig": "CSI_Demo_Dals"
        }
      };
    }
  }

  private showErrorMessage(error: HttpErrorResponse): void {
    Log.error(this.logPrefix + "ION API Error: " + JSON.stringify(error));
    this.setBusy(false);
    this.messageService
      .error({
        title: "Error " + error.status,
        message: "Failed to call ION API",
        buttons: [
          {
            text: "Close",
            isDefault: true
          }
        ]
      })
      .open();
  }

  private setBusy(isBusy: boolean): void {
    // Show the indeterminate progress indicator when the widget is busy by changing the widget state.
    this.widgetContext.setState(
      isBusy ? WidgetState.busy : WidgetState.running
    );
  }

  onRendered(event: Event) {
    this.logInfo("Rendered listview: " + event);
  }

  onSelected(event: Event) {
    this.logInfo("Selected item: " + event);
  }

  onSorted(event: Event) {
    this.logInfo("Sorted: " + event);
  }

  private logInfo(message: string, ex?: {}): void {
    Log.info(this.logPrefix + message, ex);
  }
}

@NgModule({
  imports: [CommonModule, SohoListViewModule],
  declarations: [CSICustomerComponent],
  entryComponents: [CSICustomerComponent]
})
export class CSICustomerModule {}
